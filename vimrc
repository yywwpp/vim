"### not compatible vi ####
set nocompatible

call plug#begin('~/.vim/plugged')
Plug 'morhetz/gruvbox'
Plug 'altercation/vim-colors-solarized'
Plug 'tomasiser/vim-code-dark'
call plug#end()

"### base setting ###
set showcmd		" Show (partial) command in status line.
set showmatch		" Show matching brackets.插入括号时，短暂地跳转到匹配的对应括号
set ignorecase		" Do case insensitive matching
set smartcase		" Do smart case matching
set incsearch		" Incremental search
set autowrite		" Automatically save before commands like :next and :make
set hidden             " Hide buffers when they are abandoned
set mouse=a		    " Enable mouse usage (all modes)
set nobackup
set noswapfile
set number
set numberwidth=1
set cursorline
"set cursorcolumn
set ruler
set laststatus=2
set autochdir
set autoread
set ai
set expandtab
set tabstop=2
set shiftwidth=2
set softtabstop=2
set encoding=utf-8
set fileencoding=utf-8
set fileencodings=uft-8
set helplang=cn
set title
set nofoldenable
set foldmethod=indent
set wildmenu
set relativenumber
set confirm

syntax enable
syntax on
filetype on
filetype plugin on
filetype plugin indent on

au BufWrite * %s/\s\+$//ge
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif


"### keymap setting ###
let mapleader = ";"
nnoremap <F2> :set paste<CR>
nnoremap <F2><F2> :set paste!<CR>
nnoremap <F4> :bdelete<CR>
nnoremap <leader>bn :bn<CR>
nnoremap <leader>bp :bp<CR>
nnoremap <leader>evv :vsplit $MYVIMRC<cr>
nnoremap <leader>sv :source $MYVIMRC<cr>
nnoremap <F12> :e $MYVIMRC<cr>
nnoremap <leader>s :w<cr>
nnoremap <leader>q :q<cr>
nnoremap <expr>0  col('.')==1?'^':'0'
inoremap <expr><cr> pumvisible() ? "\<c-y>" : "\<cr>"


"### UI setting ###
"""vim code dark theme
"colorscheme codedark



"set background=dark
"colorscheme gruvbox
""let g:gruvbox_termcolors=256
"let g:gruvbox_contrast_dark="soft"

"set background=light
"let g:solarized_termcolors=256
"colorscheme solarized





