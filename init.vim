"Plug#begin(PLUGINS-DIR)插件下载目录或者直接省略
"default plugin dir in '~/.config/nvim/plugged'
call plug#begin()
Plug 'lvht/phpcd.vim', { 'for': 'php', 'do': 'composer install' }
Plug 'editorconfig/editorconfig-vim'
Plug 'Yggdroot/LeaderF', { 'do': './install.sh' }

"coc.vim
Plug 'neoclide/coc.nvim', {'branch': 'release'}

"符号补全
Plug 'Raimondi/delimitMate'

"theme
Plug 'morhetz/gruvbox'
Plug 'sainnhe/vim-color-desert-night'
call plug#end()

let g:python_host_prog = "/usr/bin/python"
let g:python3_host_prog = "/usr/local/bin/python3"

"基本设置
set title
set number
set numberwidth=1
set termguicolors
set noswapfile
set nowritebackup
set showmatch
set smartcase
set ignorecase
set autowrite
"set cursorline
set list
set expandtab
set shiftwidth=4
set tabstop=4
set linebreak
set undofile
set relativenumber

"theme
colorscheme gruvbox
let g:gruvbox_contrast_dark='soft'
"colorscheme desert-night

"快捷键
let mapleader="\<space>"
nnoremap <leader>bd :bd<cr>
nnoremap <leader>s :w<cr>
nnoremap <F12> :e $MYVIMRC<CR>
tnoremap <Esc> <C-\><C-n>
nnoremap <expr>0  col('.')==1?'^':'0'
inoremap <expr><cr> pumvisible() ? "\<c-y>" : "\<cr>"
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<TAB>"

"清除尾部空格
au BufWrite * %s/\s\+$//ge
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

"Newtrw
let g:netrw_browse_split=4
let g:netrw_winsize=25
nnoremap <F1> :Vexplore $PWD<cr>

"php 代码补全
function! PhpTabskey()
    let l:col = col('.')-1
    if pumvisible()
        call feedkeys("\<c-n>",'n')
    elseif col!=0 && (getline('.')[l:col-1])!=' '
        call feedkeys("\<c-x>\<c-o>",'n')
    else
        call feedkeys("\<tab>",'n')
    endif
    return ''
endfunction
autocmd FileType php inoremap <expr><tab> PhpTabskey()

"coc.vim配置
let g:coc_node_path="/usr/local/bin/node"
function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction
autocmd FileType vim,ts,c,cpp,json,css,html
    \ inoremap <silent><expr><TAB>
    \ pumvisible() ? "\<C-n>" :
    \ <SID>check_back_space() ? "\<TAB>" :
    \ coc#refresh()




"-------------------
function! Testa(str)
    echom '====:'.a:str
    echom &filetype
    let l:filetype = &filetype
    if l:filetype == 'vim'
        echom 'this is vimscript file'
    endif
    return ''
endfunction
"inoremap <expr><S-TAB> Testa('dddddd')

